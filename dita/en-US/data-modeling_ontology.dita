<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept PUBLIC "-//c-rex//DTD DITA c-rex doctypes concept//EN" "c-rex.common.concept.dtd">
<concept id="data_modeling_ontology" xml:lang="en-US">
            <title translate="no">Data-modeling Ontology</title><prolog>
    <relations>
        <parties>
            <content-creator party-ref="parties.c-rex-maw"/>
            <content-creator party-ref="parties.industry40-dtc"/>
        </parties>
        <td-processes>
            <td-process td-process-ref="td-processes.publishing.industry40"/>
        </td-processes>
    </relations>
</prolog>
            <conbody><section>
                <lq id="lq_imn_yz3_nrb" translate="yes">
                    A data-modeling ontology is an <xref href="ontology.dita#ontology">ontology</xref> that is also a conceptual <xref href="data_model.dita#data_model">data model</xref>.
                </lq>
                <p>Data-modeling ontologies describe real-world <xref href="https://en.wikipedia.org/wiki/Universal_(metaphysics)" format="html" scope="external">universals</xref> using data structures and datatypes in a particular <xref href="data_modeling_paradigm.dita#data_modeling_paradigm">data-modeling paradigm</xref>. Ideally, they also include precise human-readable definitions of the universals that they are describing.</p>
                <p>An incomplete list of examples of data-modeling ontologies relevant to digital twins includes:</p>
                <ul id="ul_jmn_yz3_nrb">
                    <li>
                        <p><xref href="https://github.com/Azure/opendigitaltwins-dtdl/blob/master/DTDL/v2/dtdlv2.md#digital-twins-definition-language" format="html" scope="external">DTDL</xref></p>
                    </li>
                    <li>
                        <p><xref href="https://en.wikipedia.org/wiki/Web_Ontology_Language" format="html" scope="external">OWL</xref></p>
                    </li>
                </ul>
                <p>Though data-modeling ontologies define data structures and datatypes that <i>can</i> be used to represent instances of the ontologically-defined entities, those data structures and types don't have to be used to store or transmit the data. Data stored in different data structures can refer to the terms defined by the ontology to clarify the semantics of the data. Data-modeling ontologies may prioritize unambiguous description of universals over optimization of data persistence.</p>
            </section></conbody>
        </concept>