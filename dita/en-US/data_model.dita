<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept PUBLIC "-//c-rex//DTD DITA c-rex doctypes concept//EN" "c-rex.common.concept.dtd">
<concept id="data_model" xml:lang="en-US">
            <title translate="no">Data Model</title><prolog>
    <relations>
        <parties>
            <content-creator party-ref="parties.c-rex-maw"/>
            <content-creator party-ref="parties.industry40-dtc"/>
        </parties>
        <td-processes>
            <td-process td-process-ref="td-processes.publishing.industry40"/>
        </td-processes>
    </relations>
</prolog>
            <conbody><section>
                <lq id="lq_xln_yz3_nrb" translate="yes">
                    A data model is a <xref href="model.dita#model">model</xref> of data that describes its structure, datatypes, and meaning.
                </lq>
                <p>A data model will use some <b>data modeling paradigm</b> that defines its approach to <b>structuring data</b>. Some examples include:</p>
                <ul id="ul_yln_yz3_nrb">
                    <li>
                        <p>The relational paradigm structures data into tables with columns and foreign-keys</p>
                    </li>
                    <li>
                        <p>The entity-relationship paradigm structures data into classes with properties and supports relationship classes to express relations.</p>
                    </li>
                    <li>
                        <p>The triple-graph paradigm structures data as a series of subject-predicate-object "triples"</p>
                    </li>
                    <li>
                        <p>The property-graph paradigm structures data as nodes in a graph with nodes defined by classes with properties and edges defined by separate classes.</p>
                    </li>
                </ul>
                <p>The <b>datatypes</b> associated with columns/properties/attributes may be "text" or "date" or "number" or more specialized datatypes such as "integer number" or "floating-point number". The specific datatypes available will vary per data-modeling language.</p>
                <p>The <b>meaning</b> of particular data structured according to the data model may be encoded by naming data modeling elements (tables/columns/classes/properties) using the names of real-world concepts. For example, particular data that lies in the part of the structure named "equipment.weight" with datatype "number" can be understood to represent the weight of a piece of physical equipment, expressed numerically. Those names may be cross-referenced with natural-language documentation providing a more-detailed description of the meaning of the modeled data. Some data-modeling languages allow additional metadata to be associated to elements, e.g. the units-of-measure for "equipment.weight".</p>
                <p>A given data model may lie somewhere along an <b>implementation spectrum</b>. It may be an implementation-neutral "logical" or "conceptual" data model or a persistence-specific data model like a SQL database schema. Often, a high-level conceptual data model is exposed through an API, but is mapped to a low-level implementation-specific data model by the software that implements the API.</p>
                <p>The <b>subject matter</b> of a data model may be the data of a <xref href="stored_representation.dita#stored_representation">stored representation</xref>. Conversely, many <xref href="stored_representation.dita#stored_representation">stored representations</xref> have a <b>data model</b> that describes their structure and meaning.</p>
                <p>The <b>medium</b> of a "data model" is a <xref href="data_modeling_language.dita#data_modeling_language">data modeling language</xref>.</p>
                <p>The <b>modeling perspective</b> of a "data model" encompasses its data-modeling paradigm, where it sits on the "implementation perspective", as well as the choices it makes to simplify the real-world subject matter into named/structured data that describes aspects of that subject matter.</p>
                <p>The following figure shows how a <xref href="data_model.dita#data_model">Data Model</xref> relates to a <xref href="stored_representation.dita#stored_representation">Stored Representation</xref> and how both relate to the physical world. </p>
            <fig id="fig_q3x_h3k_nrb">
                <image href="../images/DataModel.png" id="image_zln_yz3_nrb">
                    <alt>Data Model</alt>
                </image>
            </fig>
                <p>Sometimes people use the unqualified term "model" to mean "data model", especially when they do "data modeling" as part of their job. They may use a term like "building model" to refer to a <b>data model</b> for creating stored representations of buildings (rather than a specific digital representation of a particular building). For this reason, it may be helpful to use a synonym like "schema", since "building schema" will be more-reliably interpreted as a "data model for structuring data that represents buildings" and never as a stored representation (aka "model") of a particular building.</p>
                <p>As an example, a stored representation of a particular real-world building will consist of data that represents relevant aspects of that building for the desired use cases. The stored representation will have a "buildings data model" that defines the concepts of real-world buildings that can be expressed in the data of the stored representation, and how they are named, structured, and represented in the stored representation's data.</p>
                <p>See <xref href="data_modeling_in_digital_twin_systems.dita#data_modeling_in_digital_twin_systems">data modeling in digital twin systems</xref>.</p>
                <p><i><b>Alternate terms</b></i></p>
                <ul id="ul_amn_yz3_nrb">
                    <li>
                        <p>"Ontology" is sometimes used as a synonym for "data model", particularly for <xref href="data-modeling_ontology.dita#data_modeling_ontology">data-modeling ontologies</xref></p>
                    </li>
                    <li>
                        <p>"Schema" is sometimes used as a synonym for "data model". DDL defines database schemas. <xref href="https://en.wikipedia.org/wiki/Open_Data_Protocol" format="html" scope="external">OData</xref> uses CSDL (Common Schema Definition Language). <xref href="https://en.wikipedia.org/wiki/RDF_Schema" format="html" scope="external">RDFS</xref> (Resource Description Framework Schema) is sometimes used in conjunction with <xref href="https://en.wikipedia.org/wiki/Web_Ontology_Language" format="html" scope="external">OWL</xref> to define data models using a <xref href="data_modeling_paradigm.dita#data_modeling_paradigm">triple-graph</xref> approach.</p>
                    </li>
                    <li>
                        <p>"Information Model" is sometimes used as a synonym for "data model", though some parties make subtle distinctions between "information model" and "data model", such as taking it to imply a data model that lies on the "conceptual" end of the implementation spectrum.</p>
                    </li>
                </ul>
                <p>The various synonyms of "data model" and the fact that some ontologies can be used as data models can be a significant source of confusion in the design and development of <xref href="stored_representation.dita#stored_representation">stored representations</xref> for <xref href="digital_twin_system.dita#digital_twin_system">digital twin systems</xref>.</p>
                <fig><image href="../images/DataModel-Ontology.png" placement="break" id="image_bmn_yz3_nrb">
                    <alt>Data Models and Ontologies</alt>
                </image></fig>
            </section></conbody>
        </concept>
