# Digital Twin Glossary DITA Representation

This is a c-rex.net project which has converted Digital Twin Glossary created by [Digital Twin Consortium](https://www.digitaltwinconsortium.org/), 
to DITA, translated into several languages and published to [iiRDS](https://iirds.org) (intelligent information Request and Delivery Standard - Tekom) Find original sources here [DTC GIT Repository](https://github.com/digitaltwinconsortium/dtc-glossary)

Original markdown resoures are converted to DITA-Map by [c-rex.net](https://c-rex.net) and translated automatically with [DeepL](https://www.deepl.com) and pre-/postprocessed by c-rex.net DTS based on segmented XLIFF and optimization scenarios.

Find more information about possiblities with c-rex.net in [c-rex.net Blog](https://blog.c-rex.net)

## Source language

Source language is English, in which the original markdown sources were written.

## Available target languages

* German
* French
* Polish
* Russian
* Simplyfied Chinese

## Formats

Repository has following formats:

* dita - conversion and translation results
* xliff - XLIFF source language (English) for translation process
* xliff-biling: bilingual xliff file - languages are translated with DeepL
* iiRDS - iiRDS Packages for data exchange with Content Delivery Portals like [c-rex.net IDS](https://ids.c-rex.net)

**Remarks for DITA concepts**

DITA Concepts are specialized for c-rex.net techcomm usages. To open this in your prefered DITA editor

* add and xml catalog file and map public doctype *"-//c-rex//DTD DITA c-rex doctypes concept//EN"* to DITA standard concept.dtd i.e. in your DITA-OT installation
* search files for 
** *DOCTYPE concept PUBLIC "-//c-rex//DTD DITA c-rex doctypes concept//EN" "c-rex.common.concept.dtd"* and replace it this string with *DOCTYPE concept PUBLIC "-//OASIS//DTD DITA Concept//EN" "concept.dtd"* 

Now you should be able to open DITA-Map and referenced concepts, but may be you will get validation errors. 
This is due to specialized metadata in prolog. Simply remove prolog information and you are done. 

## Content Delivery

iiRDS packages are automatically published to c-rex.net IDS and available under following links:


* [Digital Twin Glossary English](https://demo01.c-rex.net/apps/ids-cdp/index.html?action=infounit&value=KTontdj0V2)
* [Digital Twin Glossary German](https://demo01.c-rex.net/apps/ids-cdp/index.html?action=infounit&value=cIbzZERZYa)
* [Digital Twin Glossary French](https://demo01.c-rex.net/apps/ids-cdp/index.html?action=infounit&value=hb09qX1xUy)
* [Digital Twin Glossary Polish](https://demo01.c-rex.net/apps/ids-cdp/index.html?action=infounit&value=YQSXEmSV1S)
* [Digital Twin Glossary Russian](https://demo01.c-rex.net/apps/ids-cdp/index.html?action=infounit&value=o7PWh2KaV7)
* [Digital Twin Glossary Simplyfied Chinese](https://demo01.c-rex.net/apps/ids-cdp/index.html?action=infounit&value=X4VdyWlttI)

## Issues
* iiRDS packages are published to c-rex.net IDS Demo System, needs to be moved to prod and integrated in c-rex.net Blog
* 